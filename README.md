# Codifi Magento 2 bug toolkit

This is a Node.js application for automatic testing the bugs of your Magento 2 Store.
It requires `Puppeteer` Node.js module to be installed.

### This toolkit consists of  
 1. Product images checker 
 2. ...

### Installation
`npm install`

# Product images checker

This checker goes through the categories specified, gathers all the product page URLs listed on each category page
and checks these conditions for each product page

 - product image is placeholder (contains "/placeholder/" in image src) 
 - response on product image url returns unexpected non-image Content-Type header

### Running
`node test-product-img.js`

Notice: when running first time on Windows 7 please allow Chromium in Firewall

### Known errors
 - `UnhandledPromiseRejectionWarning: Error: net::ERR_NAME_NOT_RESOLVED`: 
   means website hostname is not found 
 - `UnhandledPromiseRejectionWarning: Error: Evaluation failed: TypeError: Cannot read property 'innerText' of null`: 
   most likely that product page you requesting is not found
 - `waiting for selector XXXX failed: timeout 100ms exceeded`:  
   most likely you specified too small timeout and you need to increase it
 - `Wrong Content-Type XXXX`:
   means Content-Type header you received is unknown and probably needs to be added to your list 
  