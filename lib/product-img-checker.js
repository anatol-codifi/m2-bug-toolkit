const clc = require('cli-color');
const fs = require('fs');
const csvWriter = require('csv-write-stream');
const puppeteerLoader = require('./puppeteer-loader');

module.exports = async(input) => {
    if (!fs.existsSync(input.config.tmpDir)) {
        fs.mkdirSync(input.config.tmpDir, 0o744);
    }

    let writer = csvWriter({
        headers: ["Sku", "Product Name", "Product Page Url", "Image", "Content-Type", "Status", "Message"]
    });
    let csvFile = input.config.tmpDir + '/' + input.config.csvFilename;
    writer.pipe(fs.createWriteStream(csvFile));

    await puppeteerLoader({
        urls: input.productUrls,
        //allowResources: ['document', 'script', 'xhr', 'fetch', 'image'],
        progressBarOptions: {
            format: '[{bar}] {percentage}% | ETA: {eta_formatted} | {value}/{total} | Url: {url}',
            etaBuffer: 100
        },
        afterLoad: async (page, url) => {
            try {
                await page.waitForSelector(input.config.imageSelector, {
                    timeout: input.config.timeoutWaitSelector
                });
            } catch (e) {
                writer.write({
                    "Sku": null,
                    "Product Name": null,
                    "Product Page Url": url,
                    "Image": null,
                    "Content-Type": null,
                    "Status": 'ERROR',
                    "Message": e.message,
                });
                return;
            }

            const result = await page.evaluate(async (
                imageSelector,
                skuSelector,
                nameSelector,
                imageContentTypes,
                placeholderHeaders
                ) => {
                    //let title = document.querySelector('h1').innerText;
                    //let price = document.querySelector('.price_color').innerText;
                    let sku = document.querySelector(skuSelector).innerText;
                    let name = document.querySelector(nameSelector).innerText;

                    //let image = document.querySelector('#button4').src;

                    let images = document.querySelectorAll(imageSelector);
                    let imagesTotal = images.length;
                    let badImages = [];
                    let imagesProcessed = [];
                    for (let image of images) {
                        let src = image.src;

                        let imageProcessed = {src: src, contentType: "", status: "OK", message: ""};

                        let response;
                        let contentType;
                        let contentLength;
                        try {
                            response = await fetch(src, {});
                            contentType = response.headers.get('Content-Type');
                            contentLength = response.headers.get('Content-Length');
                            imageProcessed.contentType = contentType;
                            if (!imageContentTypes.includes(contentType)) {
                                throw 'Wrong Content-Type ' + contentType;
                            }
                            if ((src.indexOf('/placeholder/') >= 0)
                                || ((contentType = placeholderHeaders.contentType) && (contentLength == placeholderHeaders.contentLength))
                            ) {
                                throw 'Placeholder instead of image';
                            }
                        } catch (e) {
                            imageProcessed.status = 'ERROR';
                            if (typeof(e) == 'string') {
                                imageProcessed.message = e;
                            } else {
                                imageProcessed.message = e.message;
                            }
                        }
                        imagesProcessed.push(imageProcessed);
                    }

                    return {
                        sku,
                        name,
                        imagesProcessed
                    }
                },
                input.config.imageSelector,
                input.config.skuSelector,
                input.config.nameSelector,
                input.config.imageContentTypes,
                input.config.placeholderHeaders
            );

            for (let imp of result.imagesProcessed) {
                writer.write({
                    "Sku": result.sku,
                    "Product Name": result.name,
                    "Product Page Url": url,
                    "Image": imp.src,
                    "Content-Type": imp.contentType,
                    "Status": imp.status,
                    "Message": imp.message,
                });
            }
        }
    });

    writer.end();
};
