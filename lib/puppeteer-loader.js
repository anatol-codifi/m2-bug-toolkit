const { Cluster } = require('puppeteer-cluster');
const cliProgress = require('cli-progress');

module.exports = async ({
    urls = [],
    beforeLoad = null,
    afterLoad = null,
    maxConcurrency = 8,
    allowResources = null,
    progressBarOptions = null,
    pageGotoOptions = {}
} = {}) => {
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: maxConcurrency,
    });

    const progressBar = progressBarOptions ? new cliProgress.SingleBar(progressBarOptions, cliProgress.Presets.shades_classic) : null;

    cluster.task(async ({ page, data: url }) => {
        if (allowResources) {
            await page.setRequestInterception(true);
            page.on('request', (request) => {
                if (allowResources.indexOf(request.resourceType()) !== -1) {
                    request.continue();
                } else {
                    request.abort();
                }
            });
        }

        if (beforeLoad) {
            await beforeLoad(page, url, cluster);
        }
        await page.goto(url, pageGotoOptions);
        if (afterLoad) {
            await afterLoad(page, url, cluster);
        }
        if (progressBar) {
            progressBar.increment({url: url});
        }
    });

    urls.forEach(url => {
        cluster.queue(url);
    });

    if (progressBar) {
        progressBar.start(urls.length, 0, {
            url: ''
        });
    }

    await cluster.idle();
    await cluster.close();

    if (progressBar) {
        progressBar.stop();
    }
};
