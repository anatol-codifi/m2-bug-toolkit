const fs  = require('fs');
const csv = require('csv-parser');

module.exports = async (input) => {
    let stream = fs.createReadStream(input.productUrlsCsvFile)
        .pipe(csv());

    let productUrls = [];
    for await (const row of stream) {
        productUrls.push(row.product_url);
    }
    return productUrls;
};
