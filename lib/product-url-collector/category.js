const puppeteerLoader = require('../puppeteer-loader');

module.exports = async (input) => {
    console.log("Gathering Product Page URLs...");

    const productUrls = [];
    await puppeteerLoader({
        urls: input.categoryUrls,
        allowResources: ['document'], // do not load js, css, images
        progressBarOptions: {
            format: '{value}: {url}'
        },
        afterLoad: async (page, url, cluster) => {
            try {
                await page.waitForSelector(input.config.arrowNextSelector, {
                    timeout: input.config.timeoutWaitSelector
                });
            } catch (e) {
                return;
            }

            // find product links
            const result = await page.evaluate(async (
                productLinkSelector
                ) => {
                    let links = document.querySelectorAll(productLinkSelector);
                    let productUrls = [];
                    for (let l of links) {
                        if (l.href) {
                            productUrls.push(l.href);
                        }
                    }
                    return {
                        productUrls
                    }
                },
                input.config.productLinkSelector
            );

            for (let u of result.productUrls) {
                productUrls.push(u);
            }

            // add next page to the queue
            if (result.productUrls.length) {
                let urlData = new URL(url);
                let pageNum = parseInt(urlData.searchParams.get('p'));
                if (isNaN(pageNum)) {
                    pageNum = 1;
                }
                pageNum++;
                urlData.searchParams.set('p', pageNum.toString());

                if (!input.config.maxPagesToFetch || pageNum <= input.config.maxPagesToFetch) {
                    cluster.queue(urlData.toString());
                }
            }
        }
    });

    return productUrls;
};
