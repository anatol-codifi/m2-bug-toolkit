const mysql = require('mysql');
const util = require('util');

module.exports = async (db) => {
    var conn = mysql.createConnection(db);

    var sql = `
    SELECT
      -- a_url_key.attribute_id
      -- status.value,
      DISTINCT CONCAT(base_url.value, url_key.value, '.html') AS product_url
    FROM eav_entity_type AS type_product
    INNER JOIN eav_attribute AS a_url_key
    ON a_url_key.entity_type_id = type_product.entity_type_id AND a_url_key.attribute_code = 'url_key'
    INNER JOIN eav_attribute AS a_status
    ON a_status.attribute_code = 'status' AND a_status.entity_type_id = type_product.entity_type_id
    INNER JOIN catalog_product_entity_int AS status
    ON status.attribute_id = a_status.attribute_id
    INNER JOIN catalog_product_entity_varchar AS url_key
    ON url_key.attribute_id = a_url_key.attribute_id 
      AND url_key.entity_id = status.entity_id 
      AND url_key.store_id = status.store_id
    INNER JOIN core_config_data AS base_url 
    ON base_url.path = 'web/unsecure/base_url' AND base_url.scope_id = 0
    WHERE
      type_product.entity_type_code = 'catalog_product' 
      AND status.value = 1
      AND status.store_id = 0
`;
    /*
    conn.connect(function(err) {
        if (err) {
            console.log(err);
        }
    });
     */

    const query = util.promisify(conn.query).bind(conn);
    const rows = await query(sql);
    //console.log(rows);

    conn.destroy();

    const productUrls = [];
    for (let row of rows) {
        productUrls.push(row.product_url);
    }

    return productUrls;
};
