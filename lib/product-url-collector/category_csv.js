const categoryCollector = require('./category');
const fs  = require('fs');
const csv = require('csv-parser');

module.exports = async (input) => {
    let stream = fs.createReadStream(input.categoryUrlsCsvFile)
        .pipe(csv());

    let categoryUrls = [];
    for await (const row of stream) {
        categoryUrls.push(row.category_url);
    }
    input.categoryUrls = categoryUrls;

    return await categoryCollector(input);
};
