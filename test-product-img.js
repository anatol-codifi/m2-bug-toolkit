/**
 * https://www.ajc-m2.a2hosted.com/
 */
/*
const input = {
    categoryUrls: [
        'https://www.ajc-m2.a2hosted.com/diagnostics-locating.html',
        'https://www.ajc-m2.a2hosted.com/drain-cleaning.html',
        'https://www.ajc-m2.a2hosted.com/pressing-82.html',
        'https://www.ajc-m2.a2hosted.com/pipe-tubing-tools.html',
        'https://www.ajc-m2.a2hosted.com/general-purpose-hand-tools.html',
        'https://www.ajc-m2.a2hosted.com/all-ridgid-tools.html',
    ],
    productUrls: [
        //'http://test235.loc/simple-1.html',
        //'http://test235.loc/simple-2.html'

        //'https://www.ajc-m2.a2hosted.com/pressing-82/tool-megapress-booster-only.html',
        //'https://www.ajc-m2.a2hosted.com/drain-cleaning/knocker-k9-204-4.html',
    ],
    config: {

        db: {
            host:     "127.0.0.1",
            port:     "3307",
            database: "test_m235",
            user:     "root",
            password: "123123qa"
        },

        arrowNextSelector: 'a.action.next',
        maxPagesToFetch: 100,
        productLinkSelector: 'a.product-item-link',
        //imageSelector: '.fotorama__stage__frame img.fotorama__img',
        imageSelector: '.fotorama__stage__frame img',

        //imageSelector: '.gallery-placeholder img.gallery-placeholder__image',
        skuSelector: 'div[itemprop="sku"]',
        nameSelector: 'span[itemprop="name"]',

        pauseBeforeWaitSelector: 0,
        timeoutWaitSelector: 50,
        imageContentTypes: [
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/tiff',
        ],
        placeholderHeaders: {
            contentType: 'image/jpeg',
            contentLength: 1692
        },

        tmpDir: 'var',
        csvFilename: 'product-images.csv',
    }
};
*/

/**
 * https://www.autorimshop.com/
 */
const input = {
    categoryUrlsCsvFile: "ars_category_urls.csv",
    categoryUrls: [],
    productUrlsCsvFile: "ars_products_urls.csv",
    productUrls: [],
    config: {

        db: {
            host:     "127.0.0.1",
            port:     "3307",
            database: "test_m235",
            user:     "root",
            password: "123123qa"
        },

        arrowNextSelector: 'a.action.next',
        maxPagesToFetch: 10,
        productLinkSelector: 'a.product-item-link',
        //imageSelector: '.fotorama__stage__frame img.fotorama__img',
        imageSelector: '.fotorama__stage__frame img',

        //imageSelector: '.gallery-placeholder img.gallery-placeholder__image',
        skuSelector: 'div[itemprop="sku"]',
        nameSelector: 'span[itemprop="name"]',

        pauseBeforeWaitSelector: 0,
        timeoutWaitSelector: 3000,
        imageContentTypes: [
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/tiff',
        ],
        placeholderHeaders: {
            contentType: 'image/jpeg',
            contentLength: 1692
        },

        tmpDir: 'var',
        csvFilename: 'product-images.csv',
    }
};

(async () => {
    //input.productUrls = await require('./lib/product-url-collector/db')(input.config.db);
    //input.productUrls = await require('./lib/product-url-collector/category')(input);
    //input.productUrls = await require('./lib/product-url-collector/category_csv')(input);
    input.productUrls = await require('./lib/product-url-collector/csv')(input);
    await require('./lib/product-img-checker')(input);
})();
